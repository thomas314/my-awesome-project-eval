<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Projets extends Model
{
    use Notifiable;

    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description' ,'image_url', 'technology', 'repo_url', 'website_url', 'category_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Users');
    }

    public function categorie()
    {
        return $this->belongsTo('App\Categories');
    }
}
