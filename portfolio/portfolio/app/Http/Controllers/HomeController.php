<?php

namespace App\Http\Controllers;

use App\Projets;
use App\Articles;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_invité()
    {
        $projects = Projets::latest()->take(3)->get();
        $articles = Articles::latest()->take(3)->get();
        return view('home', compact('projects', 'articles'));
    }

    public function index_auth()
    {
        $projects = Projets::latest()->take(3)->get();
        $articles = Articles::latest()->take(3)->get();
        return view('home', compact('projects', 'articles'));
    }
}
