<?php

namespace App\Http\Controllers;

use App\Projets;
use App\Categories;
use Illuminate\Http\Request;

class ProjetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projets::latest()->paginate(5);
  
        return view('projets.index',compact('projects'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        return view('projets.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projects = Projets::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image_url' => $request->input('image_url'),
            'technology' => $request->input('technology'),
            'repo_url' => $request->input('repo_url'),
            'website_url' => $request->input('website_url'),
            'category_id' => $request->input('category_id')
            
        ]);
        $projects->save();
   
        return redirect('/projets')->with('success', 'Le projet a été ajouté à la liste ! ');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Projets::findOrFail($id);
        $category= Categories::all();
        return view('projets.show', compact('project','category'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Projets::findOrFail($id);
        $categories = Categories::all();
        return view('projets.edit', compact('project','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $projects = Projets::whereId($id)->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image_url' => $request->input('image_url'),
            'technology' => $request->input('technology'),
            'repo_url' => $request->input('repo_url'),
            'website_url' => $request->input('website_url'),
            'category_id' => $request->input('category_id')
            
        ]);
        // $projects->save();
        return redirect('/projets')->with('success', 'Le projet a été modifié avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Projets::findOrFail($id);
        $project->delete();

        return redirect('/projets')->with('info', 'Ce projet a bien été supprimé');
    }
}
