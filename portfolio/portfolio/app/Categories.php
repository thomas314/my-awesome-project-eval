<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Categories extends Model
{
    use Notifiable;

    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug','name'
    ];

    public function user()
    {
        return $this->belongsTo('App\Users');
    }

    public function projets()
    {
        return $this->hasMany('App\Projets');
    }
}
