<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Articles extends Model
{
    use Notifiable;

    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','description' ,'image_url', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Users');
    }
}
