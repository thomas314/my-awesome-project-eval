@extends('layouts.app')

@section('content')
@auth
<a class="btn btn-success" href="{{ route('projets.create') }}"><i class="fas fa-plus-circle">Ajouter</i></a>
@endauth
<div class="container">
  <div class="row">
    @foreach ($projects as $project)
    <div class="col">
      <div class="card" style="width: 18rem;">
        <img src="{{$project->image_url}}" class="card-img-top" alt="{{$project->name}}">
          <div class="card-body">
            <h5 class="card-title">{{$project->name}}</h5>
            <p class="card-text">{{ Illuminate\Support\Str::limit($project->description, 350, $end='[......]') }}</p>
            <a href="{{ route('projets.show',$project->id) }}" class="btn btn-primary">Voir +</a>
              @auth
              <a href="{{ route('projets.edit', $project->id)}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
              <form action="{{ route('projets.destroy', $project->id)}}" method="post">
                @csrf
                @method('DELETE')
                <a><button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></button></a>
              </form>
              @endauth
          </div>
      </div>
    </div>
    @endforeach

  </div>
</div>
{!! $projects->links() !!}
@endsection