@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Ajouter un projet
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('projets.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Nom du projet:</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label for="description">Description du projet :</label>
              <textarea rows="5" columns="5" class="form-control" name="description"></textarea>
          </div>
          <div class="form-group">
              <label for="image_url">URL de l'image souhaitée :</label>
              <input type="text" class="form-control" name="image_url" placeholder="Rentrez un lien issu d'un navigateur pour charger une image :)"/>
          </div>
          <div class="form-group">
            <label for="technology">Technologie principale du projet :</label>
            <input type="text" class="form-control" name="technology"/>
          </div>
          <div class="form-group">
            <label for="repo_url">URL du repo :</label>
            <input type="text" class="form-control" name="repo_url"/>
          </div>
          <div class="form-group">
            <label for="website_url">URL du site :</label>
            <input type="text" class="form-control" name="website_url"/>
          </div>
          <div class="form-group">
            <label for="category_id">Catégorie :</label>
            <select class="form-control" name="category_id">
            @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
            </select>
        </div>
          <button type="submit" class="btn btn-primary">Add Data</button>
      </form>
  </div>
</div>
@endsection