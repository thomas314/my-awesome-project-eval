@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="p-3 mb-2 bg-dark text-white">
        <img class='img-fluid rounded-circle' src="{{$project->image_url}}" alt="{{$project->name}}">
        <p>Nom : {{$project->name}}</p>
        <p>Description brève du projet : {{$project->description}}</p>
        <p>Technologie principale du projet : {{$project->technology}}</p>
        @if($project->repo_url == '' || $project->repo_url === NULL)
        <p>Il n'y a aucun repo pour ce projet</p>
        @else 
        <p>Repo git du projet : <a href="{{$project->repo_url}}">{{$project->repo_url}}</a></p>
        @endif
        @if($project->website_url == '' || $project->website_url === NULL )
        <p>Il n'y a aucun URL pour ce projet</p>
        @else 
        <p>URL du projet : <a href="{{$project->website_url}}">{{$project->website_url}}</a></p>
        @endif
        @foreach ($category as $cat)
            @if($project->category_id == $cat->id)
                <span class="badge badge-info">{{$cat->name}}</span>
            @endif
        @endforeach
        </div>
    </div>
</div>
@endsection