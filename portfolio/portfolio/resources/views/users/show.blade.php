@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="p-3 mb-2 bg-dark text-white">
        <img class='img-fluid' src="{{$user->avatar}}" alt="{{$user->name}}">
        <p>Pseudo : {{$user->name}}</p>
        @if($user->first_name == '' || $user->first_name === NULL)
        <p><i>Prénom non renseigné<i></p>
        @else 
        <p>Prénom : {{$user->first_name}}</p>
        @endif 

        <p>Email : {{$user->email}}</p>
        @if($user->tel == '' || $user->tel === NULL)
        <p><i>Numéro non renseigné<i></p>
        @else
        <p>Téléphone : {{$user->tel}}</p>
        @endif 
        
        @if($user->adress == '' || $user->adress === NULL)
        <p><i>Adresse non renseignée<i></p>
        @else 
        <p>Adresse postale : {{$user->adress}}</p>
        @endif 
        </div>
        <a class="btn btn-warning" href="{{ route('users.edit',$user->id) }}"><i class="fas fa-edit">Editer profil</i></a>
    </div>
</div>
@endsection