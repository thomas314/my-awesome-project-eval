@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Modifier mes informations personnelles
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('users.update', $user->id ) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Nom (pseudo):</label>
              <input type="text" class="form-control" name="name" value="{{$user->name}}"/>
          </div>
          <div class="form-group">
              <label for="first_name">Prénom :</label>
              <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}"/>
          </div>
          <div class="form-group">
              <label for="email">Email :</label>
              <input type='email' class="form-control" name="email" value="{{$user->email}}"/>
          </div>
          <div class="form-group">
            <label for="avatar">Avatar utilisé :</label>
            <input type="text" class="form-control" name="avatar" value="{{$user->avatar}}"/>
          </div>
          <div class="form-group">
            <label for="tel">N° de téléphone :</label>
          <input type="tel" name="tel" value="{{$user->tel}}">
          </div>
          <div class="form-group">
            <label for="adress">Adresse postale :</label>
            <input type="text" name="adress" value="{{$user->adress}}">
          </div>
          <button type="submit" class="btn btn-primary">Modifier mes infos</button>
      </form>
  </div>
</div>
@endsection