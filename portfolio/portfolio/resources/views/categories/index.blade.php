@extends('layouts.app')

@section('content')
@auth 

<a class="btn btn-success" href="{{ route('categories.create') }}"><i class="fas fa-plus-circle">Ajouter catégorie</i></a>
<h2>Liste des catégories: </h2>
<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Nom</th>
        <th>Slug</th>
        <th>Action</th>
    </tr>
    @foreach ($categories as $categorie)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $categorie->name }}</td>
        <td>{{ $categorie->slug }}</td>
        <td>
            <form action="{{ route('messages.destroy',$categorie->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('messages.show',$categorie->id) }}">Voir</a>

                <a class="btn btn-warning" href="{{ route('messages.edit',$categorie->id) }}">Editer</a>

                @csrf
                @method('DELETE')
  
                <button type="submit" class="btn btn-danger">Suppr.</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $categories->links() !!}
@endauth
@endsection