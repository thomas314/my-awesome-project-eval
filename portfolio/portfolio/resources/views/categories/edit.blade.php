@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Modifier la catégorie
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('categories.update', $categorie->id ) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Nom de la catégorie :</label>
          <input type="text" class="form-control" name="name" value="{{$categorie->name}}"/>
          </div>
          <div class="form-group">
              <label for="slug">Slug :</label>
          <input type="text" name="slug" value="{{$categorie->slug}}">
          </div>
          <button type="submit" class="btn btn-primary">Modifier cette catégorie</button>
      </form>
  </div>
</div>
@endsection