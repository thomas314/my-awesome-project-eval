@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Créer une catégorie
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('categories.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Nom de la catégorie :</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
             <label for="slug">Slug :</label>
             <input type="name" name="slug">
          </div>
          <button type="submit" class="btn btn-primary">Créer catégorie</button>
      </form>
  </div>
</div>
@endsection