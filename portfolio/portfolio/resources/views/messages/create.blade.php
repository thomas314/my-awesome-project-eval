@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Formulaire de contact
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('messages.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Nom de l'expéditeur (facultatif):</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label for="email">Email :</label>
             <input type="email" name="email" required>
          </div>
          <div class="form-group">
              <label for="message">Message à envoyer :</label>
              <textarea name="message" cols="30" rows="10" placeholder="Message..."></textarea>
          </div>
          <button type="submit" class="btn btn-primary">Envoyer formulaire</button>
      </form>
  </div>
</div>
@endsection