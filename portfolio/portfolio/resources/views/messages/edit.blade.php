@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Modifier le message d'un expéditeur
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('messages.update', $message->id ) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Nom de l'article :</label>
          <input type="text" class="form-control" name="name" value="{{$message->name}}"/>
          </div>
          <div class="form-group">
              <label for="email">Email :</label>
          <input type="email" name="email" value="{{$message->email}}">
          </div>
          <div class="form-group">
              <label for="message">Message à modifier:</label>
              <textarea rows="5" columns="5" class="form-control" name="message">{{$message->message}}</textarea>
          </div>
          <button type="submit" class="btn btn-primary">Modifier ce message</button>
      </form>
  </div>
</div>
@endsection