@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="p-3 mb-2 bg-dark text-white">
        @if($message->name == '' || $message->name === NULL)
        <p>Expéditeur anonyme</p>
        @else 
        <p>Nom : {{$message->name}}</p>
        @endif
        <br>
        <p>Email de l'expéditeur : {{$message->email}}</p>
        <br>
        <p>Message de l'expéditeur: <br>
            {{$message->message}}</p>
        </div>
    </div>
</div>
@endsection