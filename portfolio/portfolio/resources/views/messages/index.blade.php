@extends('layouts.app')

@section('content')
@auth 
<h2>Liste des messages reçus</h2>
<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Nom (expéditeur)</th>
        <th>Email</th>
        <th>Message</th>
        <th>Action</th>
    </tr>
    @foreach ($messages as $message)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $message->name }}</td>
        <td>{{ $message->email }}</td>
        <td>{{ $message->message }}</td>
        <td>
            <form action="{{ route('messages.destroy',$message->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('messages.show',$message->id) }}">Voir</a>

                <a class="btn btn-warning" href="{{ route('messages.edit',$message->id) }}">Editer</a>

                @csrf
                @method('DELETE')
  
                <button type="submit" class="btn btn-danger">Suppr.</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $messages->links() !!}
@endauth

@guest
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Information</div>

                <div class="card-body">
                    <p>Votre message a été soumis au propriétaire de ce site</p>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('home') }}"> Retour vers l'accueil</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endguest
@endsection