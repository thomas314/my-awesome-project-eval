@extends('layouts.app')

@auth
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Information</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Vous êtes connecté en tant qu'utilisateur du site
                </div>
            </div>
        </div>
    </div>
</div>
<hr><br><br>
<h3>Mes derniers projets</h3>
<div class="container">
    <div class="row">
      @foreach ($projects as $project)
      <div class="col">
        <div class="card" style="width: 18rem;">
          <img src="{{$project->image_url}}" class="card-img-top" alt="{{$project->name}}">
            <div class="card-body">
              <h5 class="card-title">{{$project->name}}</h5>
              <p class="card-text">{{ Illuminate\Support\Str::limit($project->description, 350, $end='[......]') }}</p>
              <div class='container'>
                <div class="row">
                    <div class="col">
                        <a href="{{ route('projets.show',$project->id) }}" class="btn btn-primary">Voir +</a>
                        <a href="{{ route('projets.edit', $project->id)}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        <form action="{{ route('projets.destroy', $project->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <a><button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></button></a>
                        </form>      
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>

<hr>
<h3>Mes derniers articles</h3>
<div class="container">
    <div class="row">
      @foreach ($articles as $article)
      <div class="col">
        <div class="card" style="width: 18rem;">
          <img src="{{$article->image_url}}" class="card-img-top" alt="{{$article->title}}">
            <div class="card-body">
              <h5 class="card-title">{{$article->title}}</h5>
              <p class="card-text">{{ Illuminate\Support\Str::limit($article->description, 350, $end='[......]') }}</p>
              <div class='container'>
                <div class="row">
                    <div class="col">
                        <a href="{{ route('articles.show',$article->id) }}" class="btn btn-primary">Lire l'article</a>
                        <a href="{{ route('articles.edit', $article->id)}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                        <form action="{{ route('articles.destroy', $article->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <a><button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></button></a>
                        </form>
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      @endforeach
  
    </div>
  </div>
@endsection
@endauth

@guest
@section('content')
<h3>Mes derniers projets</h3>
<div class="container">
    <div class="row">
      @foreach ($projects as $project)
      <div class="col">
        <div class="card" style="width: 18rem;">
          <img src="{{$project->image_url}}" class="card-img-top" alt="{{$project->name}}">
            <div class="card-body">
              <h5 class="card-title">{{$project->name}}</h5>
              <p class="card-text">{{ Illuminate\Support\Str::limit($project->description, 350, $end='[......]') }}</p>
              <div class='container'>
                <div class="row">
                    <div class="col">
                        <a href="{{ route('projets.show',$project->id) }}" class="btn btn-primary">Voir +</a>      
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
<hr>
<h3>Mes derniers articles</h3>
<div class="container">
    <div class="row">
      @foreach ($articles as $article)
      <div class="col">
        <div class="card" style="width: 18rem;">
          <img src="{{$article->image_url}}" class="card-img-top" alt="{{$article->title}}">
            <div class="card-body">
              <h5 class="card-title">{{$article->title}}</h5>
              <p class="card-text">{{ Illuminate\Support\Str::limit($article->description, 350, $end='[......]') }}</p>
              <div class='container'>
                <div class="row">
                    <div class="col">
                        <a href="{{ route('articles.show',$article->id) }}" class="btn btn-primary">Lire l'article</a>      
                    </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      @endforeach
  
    </div>
  </div>
@endsection
@endguest

