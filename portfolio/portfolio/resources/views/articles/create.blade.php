@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-header">
    Ajouter un projet
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('articles.store') }}">
          <div class="form-group">
              @csrf
              <label for="title">Nom de votre article:</label>
              <input type="text" class="form-control" name="title"/>
          </div>
          <div class="form-group">
              <label for="description">Description de l'article:</label>
              <textarea rows="5" columns="5" class="form-control" name="description"></textarea>
          </div>
          <div class="form-group">
              <label for="image_url">URL de l'image souhaitée pour votre article :</label>
              <input type="text" class="form-control" name="image_url" placeholder="Rentrez un lien d'image issu d'un navigateur pour charger quelque chose :)"/>
          </div>
          <div class="form-group">
            <label for="user_id">Publié par:</label>
            <select class="form-control" name="user_id">
            @foreach($users as $user)
            <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
            </select>
        </div>
          <button type="submit" class="btn btn-primary">Créer article</button>
      </form>
  </div>
</div>
@endsection