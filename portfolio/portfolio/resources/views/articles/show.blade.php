@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center">
        <div class="p-3 mb-2 bg-dark text-white">
        <img class='img-fluid rounded-circle' src="{{$article->image_url}}" alt="{{$article->title}}">
        <p>Nom : {{$article->title}}</p>
        <p>Description brève du projet : {{$article->description}}</p>
        @foreach ($user as $user)
            @if($article->user_id == $user->id)
                <span class="badge badge-info">{{$user->name}}</span>
            @endif
        @endforeach
        </div>
    </div>
</div>
@endsection