@extends('layouts.app')

@section('content')
@auth
<a class="btn btn-success" href="{{ route('articles.create') }}"><i class="fas fa-plus-circle">Ajouter</i></a>
@endauth
<div class="container">
  <div class="row">
    @foreach ($articles as $article)
    <div class="col">
      <div class="card" style="width: 18rem;">
        <img src="{{$article->image_url}}" class="card-img-top" alt="{{$article->title}}">
          <div class="card-body">
            <h5 class="card-title">{{$article->title}}</h5>
            <p class="card-text">{{ Illuminate\Support\Str::limit($article->description, 350, $end='[......]') }}</p>
            <div class='container'><a href="{{ route('articles.show',$article->id) }}" class="btn btn-primary">Lire l'article</a>
              @auth
              <a href="{{ route('articles.edit', $article->id)}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
              <form action="{{ route('articles.destroy', $article->id)}}" method="post">
                @csrf
                @method('DELETE')
                <a><button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></button></a>
              </form>
              @endauth
            </div>
          </div>
      </div>
    </div>
    @endforeach

  </div>
</div>
{!! $articles->links() !!}
@endsection