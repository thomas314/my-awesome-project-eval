<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index_invité')->name('home');
Route::get('/home', 'HomeController@index_auth')->name('home');

Route::resource('projets', 'ProjetController');
Route::resource('articles', 'ArticleController');
Route::resource('messages', 'MessageController');
Route::resource('categories', 'CategorieController');
Route::resource('users', 'UserController');



